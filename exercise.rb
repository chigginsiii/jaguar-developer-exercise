class Exercise

  # Assume that "str" is a sequence of words separated by spaces.
  # Return a string in which every word in "str" that exceeds 4 characters is replaced with "marklar".
  # If the word being replaced has a capital first letter, it should instead be replaced with "Marklar".
  def self.marklar(str)
    str.split(/ +/).map { |word|
      if word.size > 4
        newWord = "marklar"
        if word =~ /^[A-Z]/
          newWord.capitalize!
        end
        if word =~ /(\W+)$/
          newWord += $1
        end
        newWord
      else
        word
      end
    }.join(' ');
  end

  # Return the sum of all even numbers in the Fibonacci sequence, up to
  # the "nth" term in the sequence
  # eg. the Fibonacci sequence up to 6 terms is (1, 1, 2, 3, 5, 8),
  # and the sum of its even numbers is (2 + 8) = 10
  @@fib_seq = [0,1]
  def self.even_fibonacci(nth)

    # have numbers all the way up to this term?
    for i in (1..nth) do
      if @@fib_seq[i].nil?
        @@fib_seq[i] = @@fib_seq[i-2] + @@fib_seq[i-1]
      end
    end

    # get the evens from this slice:
    evens = @@fib_seq.slice(1..nth).select{|i| i % 2 == 0 }
    return evens.reduce(:+)

  end

end
